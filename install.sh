#!/usr/bin/env bash

ask () {
    read -r -p "$1 [y/N]: " response
    if [[ "$response" =~ ^([yY][eE][sS]|[yY])+$ ]]; then
        return 0
    else
        return 1
    fi
}

install_fish () {

    if (ask "[?] Do you want to install the fish shell?"); then

        prefix='sudo'

        if [[ $(id -u) -eq 0 ]]; then
            prefix=''
        fi

        # Handling Debian and macOS only
        distribution=''
        if [[ $(uname -a) =~ (.*)Darwin(.*) ]]; then
            distribution='macos'
        fi
        if [[ -f /etc/debian_version ]]; then
            distribution='debian'
        fi

        if [[ -z "$distribution" ]]; then
            echo "[!] You don't seem to be on either Debian or macOS"
            return 1
        fi

        # DEBIAN
        if [[ "$distribution" -eq "debian" ]]; then

            echo "[+] Installing fish..."

            # /!\ MUTATION /!\
            $prefix apt-get update
            $prefix apt-get install fish
            # /!\ MUTATION /!\


        # MACOS
        elif [[ "$distribution" -eq "macos" ]]; then

            if [[ ! $(which brew) ]]; then
                echo "[!] Please install homebrew first:"
                echo "[!]   /usr/bin/ruby -e \"\$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)\""
                return 1
            fi

            # /!\ MUTATION /!\
            brew install fish
            # /!\ MUTATION /!\

        fi


        if [[ ( "$?" -eq 0 ) && ( $(which fish) ) ]]; then
            echo "[+] Installed fish"
            return 0
        else
            echo "[!] Couldn't install fish"
            return 1
        fi

    fi

}

install_omf () {

    if (ask "[?] Do you want to install OhMyFish?"); then
        # /!\ MUTATION /!\
        curl -L https://get.oh-my.fish > /tmp/omf
        # /!\ MUTATION /!\
        fish --init-command="set NONINTERACTIVE 1" /tmp/omf # --init-command prevent from running fish after installation

        if [[ ( "$?" -eq 0 ) && ( $(echo "omf" | fish) ) ]]; then
            echo "[+] Installed omf"
            return 0
        else
            echo "[!] Couldn't install omf"
            return 1
        fi
    fi
}

populate_fish () {

    if (ask "[?] Do you want to populate fish with custom functions? (WARNING: overwrites)"); then

        mkdir -p ~/.config/fish/functions > /dev/null
        cp -afv $tempdir/.config/fish/* ~/.config/fish/

        if [[ "$?" -eq 0 ]]; then
            echo "[+] Populated fish"
            return 0
        else
            echo "[!] Couldn't populate fish"
            return 1
        fi
    fi
}

populate_omf () {

    if (ask "[?] Do you want to populate OhMyFish with theme & packages? (WARNING: overwrites)"); then

        cp -afv $tempdir/.config/omf/* ~/.config/omf/
        echo "omf install" | fish

        if [[ "$?" -eq 0 ]]; then
            echo "[+] Populated omf"
            return 0
        else
            echo "[!] Couldn't populate omf"
            return 1
        fi
    fi
}

change_shell () {

    if (ask "[?] Do you want to change your default shell?"); then

        chsh -s $(which fish)

        if [[ "$?" -eq 0 ]]; then
            echo "[+] Your modifications will be effective at next login"
        fi
    fi
}

install_nano_hl () {

    if (ask "[?] Do you want to install nano syntax highlighting?"); then
        curl https://raw.githubusercontent.com/scopatz/nanorc/142ee236d31969f960541fbd630ed55f6fb54bbf/install.sh | sh
        if [[ "$?" -eq 0 ]]; then
            echo "[+] Done"
        fi
    fi
}


if (ask "[?] Continue and start configuration?"); then

    echo "[+] Fetching needed files"
    tempdir=$(mktemp -d /tmp/utils-XXXXXX)
    git clone https://gitlab.com/raphaelyancey/utils $tempdir

    if ! (which fish > /dev/null); then
        install_fish
    fi

    if (which fish > /dev/null); then
        populate_fish
        install_omf
        if (fish -c "functions" | grep omf > /dev/null); then
            populate_omf
        fi
        change_shell
    fi
    
    install_nano_hl

    rm -rf $tempdir
    echo "[+] Bye!"
fi


exit $?

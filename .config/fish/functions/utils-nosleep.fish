function utils-nosleep --description "Prevents macOS machine from sleeping or hibernate"
  if type -q pmset
      pmset noidle
  else
    echo "This machine doesn't seem to be running macOS, aborting."
  end
end
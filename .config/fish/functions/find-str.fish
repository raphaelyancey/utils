function find-str --description "Alias for `grep -rnw '/path/to/somewhere/' -e 'pattern'`"
  echo "Searching for $argv[2] in $argv[1]..."
  grep -rnwI $argv[2] $argv[1]
end
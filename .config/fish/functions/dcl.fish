function dcl --description "Alias for `docker-compose logs -f $argv`"
  if type -q docker-compose
      docker-compose logs -f $argv
  end
end
function nnn --description "Cross-platform notification"
  if type -q notify
    if test -z $argv
      set msg "Your command finished"
    else
      set msg $argv
    end
    notify -m $msg
  else
    echo "Couldn't find `notify` :"
    echo "  npm i -g node-notifier-cli"
  end
end

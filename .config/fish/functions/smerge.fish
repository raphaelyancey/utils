function smerge --description "Open Sublime Merge"
  set sublime_path "/Applications/Sublime Merge.app/Contents/SharedSupport/bin/smerge"
  if uname | grep Darwin > /dev/null; and test -f $sublime_path
    eval (string replace " " "\ " $sublime_path) $argv
  else
    echo "You are not on a macOS machine or Sublime Merge is not installed (https://www.sublimemerge.com)"
  end
end

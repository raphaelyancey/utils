function pullutils --description "Pull ~/.config/fish and ~/.config/omf from raphaelyancey/utils"
  set previous (pwd)
  set tempdir (mktemp -d /tmp/utils-XXXXXXX)
  git clone https://gitlab.com/raphaelyancey/utils $tempdir
  cp -afR $tempdir/.config/omf/* ~/.config/omf/
  cp -afR $tempdir/.config/fish/* ~/.config/fish/
  rm -rf $tempdir
  cd $previous
end
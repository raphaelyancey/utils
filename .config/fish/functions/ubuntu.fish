function ubuntu --description 'Launches a Docker Ubuntu container'
  docker pull ubuntu:latest
	docker run -it --rm $argv ubuntu:latest bash
end

function utils-systemd-service --description "Interactive tool to create a systemd"
  sudo bash ~/.config/fish/functions/utils-systemd-service.sh $argv
end
function prepare-sdcard --description "Prepare a SD card for RPi (wifi & ssh)"
  read -P "FreeMR passphrase? " passphrase
  touch /Volumes/boot/ssh
  echo "country=FR
ctrl_interface=DIR=/var/run/wpa_supplicant GROUP=netdev
update_config=1
network={
    ssid=\"FreeMR\"
    scan_ssid=1
    psk=\"$passphrase\"
    key_mgmt=WPA-PSK
}" > /Volumes/boot/wpa_supplicant.conf
end
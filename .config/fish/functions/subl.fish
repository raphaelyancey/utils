function subl --description "Open Sublime Text"
  set sublime_path "/Applications/Sublime Text.app/Contents/SharedSupport/bin/subl"
  if uname | grep Darwin > /dev/null; and test -f $sublime_path
    eval (string replace " " "\ " $sublime_path) $argv
  else
    echo "You are not on a macOS machine or Sublime Text is not installed (https://www.sublimetext.com)"
  end
end

function debian --description 'Launches a Docker Debian container'
  docker pull debian:latest
	docker run -it --rm $argv debian:latest bash
end

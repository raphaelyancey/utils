function pushutils --description "Push ~/.config/fish and ~/.config/omf to raphaelyancey/utils"
  set previous (pwd)
  set tempdir (mktemp -d /tmp/utils-XXXXXXX)
  git clone git@gitlab.com:raphaelyancey/utils.git $tempdir
  cp -a ~/.config/omf $tempdir/.config
  cp -a ~/.config/fish $tempdir/.config
  find $tempdir -type f -name fishd.\* -delete # Clean fish local files (gives information on hosts, not good)
  cd $tempdir
  git add -A
  git commit -m "Update"
  git push origin master
  cd $previous
  rm -rf $tempdir
end
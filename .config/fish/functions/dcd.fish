function dcd --description "Alias for `docker-compose down`"
  if type -q docker-compose
    if test -z (echo $argv) > /dev/null
      # Classic down if no argv specified
      docker-compose down
    else
      # Emulated down (stop + rm) for one app if argv
      for i in (seq 1 (count $argv))
        docker-compose rm --stop $argv[$i]
      end
    end
  end
end
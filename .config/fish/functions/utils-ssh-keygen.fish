function utils-ssh-keygen --description "ssh-keygen with `-t rsa -b 4096` defaults"
  read -l -P "Email to generate the key (RSA 4096) for: " response
  if test -z $response
    echo "Aborting."
    return 1
  else
    ssh-keygen -t rsa -b 4096 -C $response
  end
end
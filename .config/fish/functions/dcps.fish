function dcps --description "Alias for `docker-compose ps`"
  if type -q docker-compose
      docker-compose ps
  end
end
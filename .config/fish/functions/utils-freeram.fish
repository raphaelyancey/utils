function utils-freeram --description "Free cached memory on Debian-based systems"
  sudo sh -c 'sync ; echo 3 >/prod/sys/vm/drop_caches'
end

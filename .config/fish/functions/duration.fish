# Defined in /var/folders/lx/j7ng72ds4zn8v7nh_yx37qzr0000gn/T//fish.jKMXMZ/duration.fish @ line 2
function duration --description 'Prints last command duration'
	set -l duration $CMD_DURATION
	set -l date (which gdate; or which date) # gdate for bsd (macos), date for gnu date
	set -l cmd "$date -u -d @(math $duration/1000) +\"%T\""
	eval $cmd
end

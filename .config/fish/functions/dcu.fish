function dcu --description "Alias for `docker-compose $argv up`"
  if type -q docker-compose
      docker-compose $argv up
  end
end